package example.rsstest;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import example.rsstest.adapters.RssAdapter;
import example.rsstest.interfaces.MainViewInterface;
import example.rsstest.models.FeedItem;

public class MainActivity extends AppCompatActivity implements MainViewInterface {
    public static final String TAG = MainActivity.class.getSimpleName();
    private MainPresenter presenter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this.getApplicationContext();
        presenter = new MainPresenter(this, context);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        presenter.requestRssfeed();
    }

    @Override
    public void fillRecycleViewData(List<FeedItem> feed) {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.users_recycler_view);
       //assert mRecyclerView != null;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        RecyclerView.Adapter mAdapter = new RssAdapter(this, feed);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            presenter.requestRssfeed();
            Log.e(TAG, "Menu \"Refresh\" clicked");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

}
