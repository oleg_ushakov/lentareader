package example.rsstest;

import android.content.Context;
import android.util.Log;

import java.util.List;

import example.rsstest.interfaces.MainPresenterInterface;
import example.rsstest.interfaces.MainViewInterface;
import example.rsstest.interfaces.RecycleViewInterface;
import example.rsstest.models.FeedItem;
import example.rsstest.models.ResponseError;
import example.rsstest.models.xml.Rss;
import retrofit2.Response;

/**
 * Created by Oleg Ushakov on 15.09.2016.
 */
public class MainPresenter implements MainPresenterInterface {
    private MainViewInterface view;
    private RecycleViewInterface recycleView;
    private RequestAPI requestAPI;
    Context context;
    public static final String TAG = MainActivity.class.getSimpleName();

    public MainPresenter(MainViewInterface view, Context context) {
        this.view = view;
        this.context = context;
        requestAPI = new RequestAPI(this, context);
    }

    public MainPresenter(RecycleViewInterface recycleView) {
        this.recycleView = recycleView;
    }

    public void requestRssfeed() {
        requestAPI.requestRss();
    }

    @Override
    public void noInternetError() {
        Log.e(TAG, context.getString(R.string.check_internet));
        //Toast.makeText(this, R.string.check_internet, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccess(Response<Rss> response) {
        requestAPI.getDataSet(response);
    }

    @Override
    public void onError(ResponseError error) {
        Log.e(TAG, context.getString(R.string.error_message) + error.message());
        //String toastString = getString(R.string.error_message) + error.message() + " : " + error.statusCode();
        //Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure(Throwable t) {
        Log.e(TAG, context.getString(R.string.error_on_failure) + t.getMessage());
    }

    @Override
    public void returnDataSet(List<FeedItem> feed) {
        view.fillRecycleViewData(feed);
    }

    public void titleClick(int position) {
        recycleView.titleClick(position);
    }

    public void imageClick(int position) {
        recycleView.imageClick();
    }
}
