package example.rsstest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;
import java.util.List;

import example.rsstest.interfaces.MainPresenterInterface;
import example.rsstest.models.FeedItem;
import example.rsstest.models.ResponseError;
import example.rsstest.models.xml.Item;
import example.rsstest.models.xml.Rss;
import example.rsstest.retrofit.ErrorUtils;
import example.rsstest.retrofit.interfaces.RssService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Oleg Ushakov on 15.09.2016.
 */
public class RequestAPI {
    private MainPresenterInterface mainPresenter;
    private Context context;

    public RequestAPI(MainPresenterInterface mainPresenter, Context context) {
        this.mainPresenter = mainPresenter;
        this.context = context;
    }

    protected void requestRss() {
        if ( !checkInternet() )
            mainPresenter.noInternetError();
        else {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RssService.API_BASE_URL)
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();
            RssService service = retrofit.create(RssService.class);
            Call<Rss> call = service.getLentaRss();

            Callback<Rss> callback = new Callback<Rss>() {
                @Override
                public void onResponse(Call<Rss> call, Response<Rss> response) {
                   //Rss rssItem = response.body(); Log.d(TAG, rssItem.toString());
                    if (response.isSuccessful()) {
                        mainPresenter.onSuccess(response);
                    } else {
                        ResponseError error = ErrorUtils.parseError(response);
                        mainPresenter.onError(error);
                    }
                }
                @Override
                public void onFailure(Call<Rss> call, Throwable t) {
                    mainPresenter.onFailure(t);
                }
            };
            call.enqueue(callback);
        }
    }

    protected void getDataSet(Response<Rss> response) {
        ArrayList<FeedItem> feed = new ArrayList<>();
        List<Item> items = response.body().getChannel().getItem();

        for (Item item : items) {
            String title = (item.getTitle() == null) ? "" : item.getTitle();
            String description = (item.getDescription() == null) ? "" : item.getDescription();
            String imgUrl = (item.getEnclosure() == null) ? "" : item.getEnclosure().getUrl();
            feed.add(new FeedItem(title, description, imgUrl));
        }
        mainPresenter.returnDataSet(feed);
    }

    private boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return ( networkInfo != null && networkInfo.isConnectedOrConnecting() ); //return true or false
    }
}
