package example.rsstest.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import example.rsstest.MainPresenter;
import example.rsstest.R;
import example.rsstest.interfaces.RecycleViewInterface;
import example.rsstest.models.FeedItem;

public class RssAdapter extends RecyclerView.Adapter<RssAdapter.ViewHolder> implements RecycleViewInterface {
    private Context mContext;
    private List<FeedItem> mDataset;
    private static MainPresenter presenter;

    public RssAdapter(Context context, List<FeedItem> dataset) {
        mContext = context;
        mDataset = dataset;
        presenter = new MainPresenter(this);
    }

    // class for receiving each element in list item
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mTitle;
        public ImageView mImg;
        //public TextView mTextView1;

        public ViewHolder(View v) {
            super(v);
            mTitle = (TextView) v.findViewById(R.id.recycle_title);
            mImg = (ImageView) v.findViewById(R.id.recycle_image);

            mTitle.setOnClickListener(this);
            mImg.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                switch (v.getId()) {
                    case R.id.recycle_title:
                        presenter.titleClick(position);
                        break;
                    case R.id.recycle_image:
                        presenter.imageClick(position);
                }
            }
        }
    }
    @Override
    public void titleClick(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mDataset.get(position).getTitle())
                .setMessage(mDataset.get(position).getDescription())
                .setCancelable(true)
                .setNegativeButton(R.string.title_dialog_close_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void imageClick() {
        //action on item image click
    }

    // Create new views
    @Override
    public RssAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rss_item, parent, false);
        return new ViewHolder(v);
    }

    // set content for each view item
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FeedItem feedItem = mDataset.get(position);
        holder.mTitle.setText(feedItem.getTitle());
        if (feedItem.getUrl() != null && !feedItem.getUrl().isEmpty())
            Picasso.with(mContext).load(feedItem.getUrl()).into(holder.mImg);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
