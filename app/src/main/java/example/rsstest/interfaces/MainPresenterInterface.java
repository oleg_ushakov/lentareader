package example.rsstest.interfaces;

import java.util.List;

import example.rsstest.models.FeedItem;
import example.rsstest.models.ResponseError;
import example.rsstest.models.xml.Rss;
import retrofit2.Response;

/**
 * Created by Oleg Ushakov on 16.09.2016.
 */
public interface MainPresenterInterface {
    void noInternetError();
    void onSuccess(Response<Rss> dataSet);
    void onError(ResponseError error);
    void onFailure(Throwable t);
    void returnDataSet(List<FeedItem> feed);

}
