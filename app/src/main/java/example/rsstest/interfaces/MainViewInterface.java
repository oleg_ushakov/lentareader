package example.rsstest.interfaces;

import java.util.List;

import example.rsstest.models.FeedItem;

/**
 * Created by Oleg Ushakov on 15.09.2016.
 */
public interface MainViewInterface {
    void fillRecycleViewData(List<FeedItem> feed);
}
