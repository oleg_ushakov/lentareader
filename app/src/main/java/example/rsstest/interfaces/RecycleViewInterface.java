package example.rsstest.interfaces;

/**
 * Created by Oleg Ushakov on 16.09.2016.
 */
public interface RecycleViewInterface {

    void imageClick();
    void titleClick(int position);
}
