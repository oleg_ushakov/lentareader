package example.rsstest.models;

/**
 * Created by Oleg Ushakov on 13.09.2016.
 */
public class FeedItem {
    String title;
    String description;
    String url;

    public FeedItem(String itemTitle, String itemDescription, String itemImageUrl) {
        this.description = itemDescription;
        this.title = itemTitle;
        this.url = itemImageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
