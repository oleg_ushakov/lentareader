package example.rsstest.models;

public class ResponseError {

    private int statusCode;
    private String message;

    public ResponseError() {
    }

    public int statusCode() {
        return statusCode;
    }
    public String message() {
        return message;
    }
}