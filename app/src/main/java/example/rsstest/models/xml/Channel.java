package example.rsstest.models.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import java.util.List;

/**
 * Created by Oleg Ushakov on 13.09.2016.
 */
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2005/Atom", prefix = "atom")
})

@Root(strict = false)
public class Channel {
    @Element(name="language", required = false)
    String language;

    @Element(name="title", required = false)
    String title;

    @Element(name="description", required = false)
    String description;

    @Path("link")
    @Text(required = false)
    String channelLink;

    @Element(name="image", required = false)
    Image image;

    @ElementList(name = "item", inline = true, required = false)
    List<Item> item;

    public String getLanguage() { return this.language; }
    public void setLanguage(String _value) { this.language = _value; }

    public String getTitle() { return this.title; }
    public void setTitle(String _value) { this.title = _value; }

    public String getDescription() { return this.description; }
    public void setDescription(String _value) { this.description = _value; }

    public String getChannelLink() { return this.channelLink; }
    public void setChannelLink(String _value) { this.channelLink = _value; }

    public Image getImage() { return this.image; }
    public void setImage(Image _value) { this.image = _value; }

    public List<Item> getItem() { return this.item; }
    public void setItem(List<Item> _value) { this.item = _value; }
}
