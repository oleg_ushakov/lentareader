package example.rsstest.models.xml;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by Oleg Ushakov on 13.09.2016.
 */
@Root(name = "enclosure", strict = false)
@Path("enclosure")

public class Enclosure {
    @Attribute(name="url", required = false)
    private String url;

    @Attribute(name="length", required = false)
    private String length;

    @Attribute(name="type", required = false)
    private String type;

    public String getUrl() { return this.url; }
    public void setUrl(String _value) { this.url = _value; }


    public String getLength() { return this.length; }
    public void setLength(String _value) { this.length = _value; }


    public String getType() { return this.type; }
    public void setType(String _value) { this.type = _value; }
}