package example.rsstest.models.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Oleg Ushakov on 13.09.2016.
 */
@Root(strict = false)
public class Image {
    @Element(name="url", required = false)
    String url;

    @Element(name="title", required = false)
    String title;

    @Element(name="link", required = false)
    String link;

    @Element(name="width", required = false)
    String width;

    @Element(name="height", required = false)
    String height;

    public String getUrl() { return this.url; }
    public void setUrl(String _value) { this.url = _value; }

    public String getTitle() { return this.title; }
    public void setTitle(String _value) { this.title = _value; }

    public String getLink() { return this.link; }
    public void setLink(String _value) { this.link = _value; }

    public String getWidth() { return this.width; }
    public void setWidth(String _value) { this.width = _value; }

    public String getHeight() { return this.height; }
    public void setHeight(String _value) { this.height = _value; }
}

