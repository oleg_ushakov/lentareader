package example.rsstest.models.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

/**
 * Created by Oleg Ushakov on 13.09.2016.
 */
@Root(strict = false)
public class Item {
    @Element(name="guid", required = false)
    String guid;

    @Element(name="title", required = false)
    String title;

    @Path("link")
    @Text(required = false)
    String link;

    @Element(name="description", required = false)
    String description;

    @Element(name="pubDate", required = false)
    String pubDate;

    @Element(name="enclosure", required = false)
    private Enclosure enclosure;

    @Element(name="category", required = false)
    String category;

    public String getGuid() { return this.guid; }
    public void setGuid(String _value) { this.guid = _value; }

    public String getTitle() { return this.title; }
    public void setTitle(String _value) { this.title = _value; }

    public String getLink() { return this.link; }
    public void setLink(String _value) { this.link = _value; }

    public String getDescription() { return this.description; }
    public void setDescription(String _value) { this.description = _value; }

    public String getPubDate() { return this.pubDate; }
    public void setPubDate(String _value) { this.pubDate = _value; }

    public Enclosure getEnclosure() { return this.enclosure; }
    public void setEnclosure(Enclosure _value) { this.enclosure = _value; }

    public String getCategory() { return this.category; }
    public void setCategory(String _value) { this.category = _value; }
}
