package example.rsstest.models.xml;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Oleg Ushakov on 13.09.2016.
 */
@Root(strict = false)
public class Link {
    @Attribute(name="rel", required = false)
    String rel;

    @Attribute(name="type", required = false)
    String type;

    @Attribute(name="href", required = false)
    String href;

    public String getRel() { return this.rel; }
    public void setRel(String _value) { this.rel = _value; }

    public String getType() { return this.type; }
    public void setType(String _value) { this.type = _value; }

    public String getHref() { return this.href; }
    public void setHref(String _value) { this.href = _value; }
}
