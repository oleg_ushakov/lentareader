package example.rsstest.models.xml;

/**
 * Created by Oleg Ushakov on 12.09.2016.
 */
    import org.simpleframework.xml.Attribute;
    import org.simpleframework.xml.Element;
    import org.simpleframework.xml.Root;

@Root
public class Rss {
    @Element(name="channel", required = false)
    Channel channel;

    @Attribute(name="version", required = false)
    String version;

    @Attribute(name="atom", required = false)
    String atom;

    public Channel getChannel() { return this.channel; }
    public void setChannel(Channel _value) { this.channel = _value; }

    public String getVersion() { return this.version; }
    public void setVersion(String _value) { this.version = _value; }
}
