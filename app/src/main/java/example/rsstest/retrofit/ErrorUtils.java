package example.rsstest.retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;

import example.rsstest.models.ResponseError;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {
    public static ResponseError parseError(Response<?> response) {
        Converter<ResponseBody, ResponseError> converter =
		    ServiceGenerator.getRetrofit().responseBodyConverter(ResponseError.class, new Annotation[0]);
        ResponseError error;

        try {
			error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ResponseError();
        }
        return error;
    }
}
