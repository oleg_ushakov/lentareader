package example.rsstest.retrofit.interfaces;

import example.rsstest.models.xml.Rss;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Oleg Ushakov on 12.09.2016.
 */
public interface RssService {
    public static final String API_BASE_URL = "https://lenta.ru/";
    @GET("rss")
        Call<Rss> getLentaRss();
}
